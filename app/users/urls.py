from django.urls import path
from .views import ListUsers

app_name = 'users'

urlpatterns = [
    path('list', ListUsers.as_view()),
]
