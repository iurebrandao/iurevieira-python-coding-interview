from django.forms import model_to_dict
from rest_framework.views import APIView
from app.users.models import User
from rest_framework import status
from rest_framework.response import Response


class ListUsers(APIView):
    def get(self, *args, **kwargs):
        users = User.objects.all()
        response = {
            'users': [{**model_to_dict(user), **{"birth_date": user.get_birth_date()}} for user in users]
        }

        return Response(response, status=status.HTTP_200_OK)
