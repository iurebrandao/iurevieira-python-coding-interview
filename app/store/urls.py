from django.urls import path
from .views import ListProducts, CreateProduct

app_name = 'store'

urlpatterns = [
    path('products', ListProducts.as_view()),
    path('create', CreateProduct.as_view()),
]
