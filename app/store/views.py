from django.forms import model_to_dict
from rest_framework.views import APIView
from app.store.models import Products
from rest_framework import status
from rest_framework.response import Response


class ListProducts(APIView):
    def get(self, *args, **kwargs):
        products = Products.objects.all()
        response = [model_to_dict(product) for product in products]
        return Response(response, status=status.HTTP_200_OK)


class CreateProduct(APIView):
    def post(self, request, *args, **kwargs):
        data = request.data.copy()
        Products.objects.create(name=data.get("name"))

        return Response({"msg": "Created succesfully!"}, status=status.HTTP_201_CREATED)
